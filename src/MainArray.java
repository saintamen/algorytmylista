public class MainArray {
    public static void main(String[] args) {
        MyArrayList mal = new MyArrayList();
        mal.addElement(1);
        mal.addElement(2);
        mal.addElement(3);
        mal.addElement(4);
        mal.addElement(5);
        mal.addElement("abrakadabra");

        mal.printList();

        System.out.println();
        System.out.println(mal.size());
        System.out.println();
        System.out.println(mal.get(0));
        System.out.println(mal.get(1));
        System.out.println(mal.get(5));
        System.out.println(mal.get(6));
//        System.out.println(mal.get(7));
    }
}
