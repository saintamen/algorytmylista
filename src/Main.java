public class Main {
    public static void main(String[] args) {
        MyLinkedList lista = new MyLinkedList();

        lista.addElement(1);
        lista.addElement(2);
        lista.addElement(3);
        lista.addElement(4);
        lista.addElement(5);
        lista.addElement("abrakadabra");

//        lista.printList();
//
//        System.out.println(lista.get(0));
//        System.out.println(lista.get(1));
//        System.out.println(lista.get(2));
//        System.out.println(lista.get(3));
//        System.out.println(lista.get(4));
//        System.out.println(lista.get(5));
//        System.out.println(lista.get(6));

        lista.printList();
        System.out.println();
        lista.removeElement(2);

        lista.printList();

//        lista.removeLastVersionWithPrevious();
//        lista.removeLastVersionWithPrevious();
//        lista.removeLastVersionWithPrevious();
//        lista.removeLastVersionWithPrevious();
//        lista.removeLastVersionWithPrevious();
//        lista.removeLastVersionWithPrevious();
//        lista.removeLast();
//        lista.removeLast();
//        lista.removeLast();
//        lista.removeLast();
//        lista.removeLast();
//        lista.removeLast();

//        lista.printList();
    }
}
