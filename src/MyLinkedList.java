public class MyLinkedList {
    private ListElement root;

    public MyLinkedList() {
        this.root = null;
    }

    public void addElement(Object data) {
        ListElement listElement = new ListElement(data);
        if (root == null) {
            root = listElement;
        } else {
            ListElement tmp = root;
            while (tmp.getNext() != null) {
                tmp = tmp.getNext();
            }

            ListElement toAdd = new ListElement(data);
            tmp.setNext(toAdd);
        }
    }

    public void printList() {
        ListElement tmp = root;
        if (root == null) return;

        System.out.println(tmp.getData());
        while (tmp.getNext() != null) {
            tmp = tmp.getNext();
            System.out.println(tmp.getData());
        }
    }

    public void removeLastVersionWithPrevious() {
        ListElement tmp = root;
        if (root == null) return;

        if (tmp.getNext() != null) {      // wiecej niz 1 element na liscie
            ListElement tmpPrev = root; // przedostatni element

            while (tmp.getNext() != null) {
                tmpPrev = tmp;
                tmp = tmp.getNext();
            }
            tmpPrev.setNext(null);
            // tmp - to ostatni element
        } else {
            root = null;
        }
    }

    public void removeLast() {
        ListElement tmp = root;
        if (root == null) return;

        if (tmp.getNext() != null) {
            while (tmp.getNext() != null && tmp.getNext().getNext() != null) {
                tmp = tmp.getNext();
            }

            tmp.setNext(null);
        } else if (tmp != null && tmp.getNext() == null) { // to znaczy ze jest tylko jeden
            // element root
            root = null;
        }
    }

    public Object get(int indeks) {
        if (root == null) throw new ArrayIndexOutOfBoundsException(); // brak elementow - blad

        ListElement tmp = root;
        int counter = 0; // licznik elementow
        while (tmp.getNext() != null && counter != indeks) {
            // dopoki istnieja elementy i nie dotarlismy do konkretnego numeru elementu powtarzaj petle
            tmp = tmp.getNext();
            counter++;
            // przejdz do nastepnego i inkrementuj licznik
        }
        // jesli licznik != indeks (wyszlismy z petli z powodu braku elementow [tmp.getNext() != null]
        // a nie dlatego ze dotarlismy do konkretnego elementu
        if (counter != indeks) {
            throw new ArrayIndexOutOfBoundsException(); // blad
        }

        return tmp.getData(); // zwróć DANE ELEMENTU a nie sam element

        // przejście do n-tego elementu listy i zwrócenie go
        // wskazówka (zrób licznik przechodzenia przez pętlę while [zeby wiedziec na ktorym
        //                                                                  jestesmy elemencie)
        // wskazówka (nie zapomnij sprawdzić czy dany element istnieje na liście)
        // wskazówka pro: wiesz co powinno się wydarzyć jeśli wykroczysz poza zakres?

//        return ?;
    }

    public void removeElement(int indeks) {
        if (root == null) throw new ArrayIndexOutOfBoundsException();

        ListElement tmp = root;
        int counter = 0; // licznik elementow
        while (tmp.getNext() != null && ((counter+1) != indeks)) {
            tmp = tmp.getNext();
            counter++;
            // przejdz do nastepnego i inkrementuj licznik
        }
        if((counter+1) == indeks){ // tmp to element przed elementem usuwanym
            if(tmp.getNext() == null){
                throw new ArrayIndexOutOfBoundsException();
            }
            tmp.setNext(tmp.getNext().getNext());
        }
    }

    public int size() {
        if (root == null) return 0;

        // w przeciwnym razie

        ListElement tmp = root;
        int counter = 1; // licznik elementow
        while (tmp.getNext() != null) {
            tmp = tmp.getNext();
            counter++;
            // przejdz do nastepnego i inkrementuj licznik
        }

        return counter;
    }
}
